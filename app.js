var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var session = require('express-session');
var app = express();

var inlineCss = require('inline-css');

// initialize express-session to allow us track the logged-in user across sessions.
app.use(session({
  key: 'user_sid',
  secret: 'somerandonstuffs',
  resave: false,
  saveUninitialized: false,
  cookie: {
    expires: 600000
  }
}));

app.use('/public', express.static('public'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use(
  session({
    cookie: {
      maxAge: 30 * 24 * 60 * 60 * 1000
    }
  })
);
// Access the parse results as request.body
app.post('/', function (req, res) {
  // console.log(request.body);
  // res.send('ceva');


  if (req.body.password && req.body.password === 'testntg') {

    req.session.isLoggedIn = 'on';
    // res.send('ceva');

    res.writeHead(302, {
      'Location': '/send-email'
    });
    res.end();
  } else {

    res.writeHead(302, {
      'Location': '/'
    });
    res.end();
  }
});

app.get('/send-email', (req, res) => {
  console.log('req.session - ', req.session.isLoggedIn);

  if (req.session.isLoggedIn && req.session.isLoggedIn === 'on') {

    res.render('send-email', {title: 'Hey', message: 'Hello there!'})
  } else {

    res.writeHead(302, {
      'Location': '/'
    });
    res.end();
  }
})
app.post('/send-email', (req, res) => {

  console.log('req.session.isLoggedIn - -- ', req.session.isLoggedIn);
  if (1 || req.session.isLoggedIn && req.session.isLoggedIn === 'on') {

    console.log(req.body);

    var view_color_tableBorder = '#777777';
    var view_color_tableBorder2 = '#dbdbdb';
    var view_color_tableBackgroundMain = '#ffffff';

    var nodemailer = require('nodemailer');

    var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: process.env.OWNER_EMAIL,
        pass: process.env.OWNER_PASSWORD
      }
    });

    const htmlButtons = `<div class="section-spacing">
<table>
<td class=""><div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="mailto:salesrep@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%20%7B%7Bquote-id%7D%7D%20%3CApproved%3E&body=Approved%0D%0A" style="height:40px;v-text-anchor:middle;width:120px;" arcsize="13%" strokecolor="#7B5456" fillcolor="#7B5456">
    <w:anchorlock/>
    <center style="color:#ffffff;font-family:sans-serif;font-size:13px;font-weight:bold;">APPROVE</center>
  </v:roundrect>
<![endif]--><!--[if !mso]>--><a href="mailto:salesrep@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%20%7B%7Bquote-id%7D%7D%20%3CApproved%3E&body=Approved%0D%0A"
style="background-color:#7B5456;border:2px solid #7B5456;border-radius:5px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:120px;-webkit-text-size-adjust:none;mso-hide:all;">APPROVE</a><!--<![endif]--></div></td>
<td class="ml-5"><div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="mailto:salesrep@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%20%7B%7Bquote-id%7D%7D%20%Rejected%3E&body=Rejected%0D%0A" style="height:40px;v-text-anchor:middle;width:100px;" arcsize="13%" strokecolor="#7B5456" fillcolor="">
    <w:anchorlock/>
    <center style="color:#7B5456;font-family:sans-serif;font-size:13px;font-weight:bold;">REJECT</center>
  </v:roundrect>
<![endif]--><!--[if !mso]>--><a href="mailto:salesrep@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%20%7B%7Bquote-id%7D%7D%20%Rejected%3E&body=Rejected%0D%0A"
style="background-color:transparent;border:2px solid #7B5456;border-radius:5px;color:#7B5456;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:100px;-webkit-text-size-adjust:none;mso-hide:all;">REJECT</a><!--<![endif]--></div></td>
<td class="ml-5"><div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="mailto:salesrep@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%20%7B%7Bquote-id%7D%7D%20%3CRequested%20%Change%3E&body=Requested change%0D%0A" style="height:40px;v-text-anchor:middle;width:221px;" arcsize="13%" strokecolor="#7B5456" fillcolor="">
    <w:anchorlock/>
    <center style="color:#7B5456;font-family:sans-serif;font-size:13px;font-weight:bold; white-space: nowrap;">REQUEST CHANGE</center>
  </v:roundrect>
<![endif]--><!--[if !mso]>--><a href="mailto:salesrep@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%20%7B%7Bquote-id%7D%7D%20%3CRequested%20%Change%3E&body=Requested change%0D%0A"
style="background-color:transparent;border:2px solid #7B5456;border-radius:5px;color:#7B5456;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:221px;-webkit-text-size-adjust:none; white-space: nowrap;">REQUEST CHANGE</a></div></td><!--<![endif]-->
</table>
</div>`;


    let theHtml = `
<!doctype html>
    <html>
      <head>
        <meta charset="utf-8">
        <style>
        body{
        background-color: #fafafa;
        }
        /** from FH **/
        .ntg-lib-button-secondary {
        
   /* base */
   -webkit-tap-highlight-color: transparent;
    display: inline-block;
    white-space: nowrap;
    text-decoration: none;
    vertical-align: baseline;
    text-align: center;
    margin: 0;
    overflow: visible;
   outline: none;
   box-sizing: border-box;
    position: relative;
   text-transform: uppercase;
    padding: 10px 20px;
    border: 2px solid #7B5456;
    font-weight: bold;
    -webkit-user-select: none;
    user-select: none;
    cursor: pointer;
    border-radius: 6px;
    font-size: 14px;
    
    background-color: transparent;
    color: #7B5456;
    
}
.ntg-lib-button-secondary a{

    color: #7B5456;
    text-decoration: none;;
}
        .ntg-lib-button-primary {
        
         /* base */
         -webkit-tap-highlight-color: transparent;
    display: inline-block;
    white-space: nowrap;
    text-decoration: none;
    vertical-align: baseline;
    text-align: center;
    margin: 0;
    overflow: visible;
             outline: none;
             box-sizing: border-box;
    position: relative;
             text-transform: uppercase;
    padding: 10px 20px;
    border: 2px solid #7B5456;
    font-weight: bold;
    -webkit-user-select: none;
    user-select: none;
    cursor: pointer;
    border-radius: 6px;
    font-size: 14px;
    
         background-color: #7B5456;
         color: #ffffff;
         

        }
.ntg-lib-button-primary a{

         color: #ffffff;
    text-decoration: none;
}
        
        .ntg-lib-button{
        margin-top: 10px;
        margin-bottom: 10px;
        }
        
.mat-focus-indicator {
    position: relative;
}

/** custom */
a{
color: #7B5456;
}

h4{
margin-bottom: 5px;;
text-transform: uppercase;;
margin-bottom: 1.5em;;
}
h6{
margin-top: 5px;
font-size: 14px;
font-weight: normal;
text-transform: uppercase;;
margin-bottom: 0.5em;;
}

        h3{ color: #797979; }
        
        h3,h5{
        
    font-family: Roboto, "Helvetica Neue", sans-serif;
        }
        
        .headers-conglomerate h5, .headers-conglomerate h3{
        margin-top:0;
        margin-bottom: 0;;
        }
        .headers-conglomerate h3{
        margin-top: 5px;
        }
        
        .flex-grid{
display:flex;
align-items: flex-end;
        }
        section{
        margin-top: 30px;
        margin-bottom: 30px;
    font-family: Roboto, "Helvetica Neue", sans-serif;
        }
        .section-spacing{
        
        margin-top: 30px;
        margin-bottom: 30px;
    font-family: Roboto, "Helvetica Neue", sans-serif;
        }
        .ntg-lib-button + .ntg-lib-button{
        margin-left: 15px;;
        }
        .ml-5{
        padding-left: 5px;
        }
        .ml-15{
        margin-left: 15px;
        }
        .quote-attribute{
        display: flex;
        }
        .quote-attribute + .quote-attribute{
        margin-top: 10px;
        }
        .quote-attribute .quote-attribute--label{
        width: 210px;
        flex: 0 0 210px;
        }
        .quote-attribute .quote-attribute--val{
        flex: 100
        }
        
        .logo{
        flex: 0 0 60px; height: 67px; max-width: 60px; width: 60px;
        }
        .logo img{
        width: 100%;
        }
        @media all and (max-width:600px) {
        .ntg-lib-button + .ntg-lib-button{
        margin-left: 5px;;
        }
        .ntg-lib-button-primary,.ntg-lib-button-secondary {
        padding: 1px 10px;
        }
}

.table-container {
  background-color: ${view_color_tableBackgroundMain};
  box-shadow: -4px 8px 10px rgba(0, 0, 0, 0.3);
  border: 1px solid ${view_color_tableBorder};
  width: 100%;
  margin-bottom: 10px;
}
/*


.table-container td{
  border-right: 1px solid ${view_color_tableBorder};
}
.table-container td:last-child{
  border-right: 0px solid ${view_color_tableBorder};
}
 */
.table-container--header {
  font-size: 16px;
  font-weight: 700;
  border-top: 10px solid ${view_color_tableBackgroundMain};
  border-bottom: 10px solid ${view_color_tableBackgroundMain};
  border-left: 10px solid ${view_color_tableBackgroundMain};
}
.table-container table {
  width: 100%;
  padding-bottom: 10px;
}
.table-container table thead tr td {
  background-color: #fafafa;
  padding-top: 10px;
  padding-bottom: 10px;
  font-weight: 700;
}
.table-container table thead tr td:first-child {
  padding-left: 10px;
}
.table-container table thead tr td:last-child {
  padding-right: 10px;
}
.table-container table tbody {
  padding-top: 10px;
}
.table-container table {

  border-top: 1px solid ${view_color_tableBorder2};
}
.table-container .tr-has-border-bottom td {
  border-bottom: 1px solid ${view_color_tableBorder2};
}
.table-container table tbody tr td:first-child {
  padding-left: 10px;
}
.table-container table tbody tr td {
  padding-top: 5px;
  background-color: #fff;
}
.ntg-email-table th{
box-sizing: border-box; text-align: left; border: 1px solid #e9ecef; border-width: 1px 0 1px 0; font-weight: normal; color: #121212; background: #f8f9fa; transition: box-shadow 0.2s; font-family: Nunito, Roboto, -apple-system, BlinkMacSystemFont, 'Segoe UI', Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; background-color: #F8F9FA; font-size: 0.75rem; text-transform: capitalize; line-height: 1rem; padding: 18px 8px; border-left: none; border-right: none; border-top: 1px solid #DBDBDB; border-bottom: 2px solid #DBDBDB;
}
.ntg-email-table .p-datatable-tbody tr td{
box-sizing: border-box; text-align: left; border-width: 0 0 1px 0; font-size: 0.875rem; font-family: Nunito, Roboto, -apple-system, BlinkMacSystemFont, 'Segoe UI', Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; line-height: 1.188rem; padding: 18px 8px; border: none;
}
.ntg-email-table .p-datatable-tbody tr{
box-sizing: border-box; background: #ffffff; color: #1C1C1C; transition: box-shadow 0.2s; outline-color: rgba(0, 0, 0, 0.12);
}
.ntg-email-table{
box-sizing: border-box; width: 100%; border-collapse: collapse;
}
</style>
      </head>
      <body>
      
<table class="body" cellpadding="0" cellspacing="0" border="0" width="100%" style="box-sizing: border-box; background: #FAFAFA; width: 100%; font-family: Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';">
  <tr style="box-sizing: border-box;">
    <td class="container" style="box-sizing: border-box; align-content: center; width: 930px; font-family: Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';" width="930">
      <div class="content" style="box-sizing: border-box; margin-top: 20px;">
      <table class="flex-grid">
      <td class="logo">
      
      <img class="logo" style="" width="60" height="auto" src="https://ntgfreight.com/wp-content/uploads/2018/09/NTG-high-res-png.png"/>
</td>
      <td style="padding-left: 15px;">
      <br>
      <br>
      <h3>Request for Quotation</h3> 
</td>
</table>
<section>
        <p>Dear <strong>{{customer-name}}</strong>, Greeting from NTG!  </p>
        <p>Below are the details of your quote request. Your quote will expire on {{date-expiry-MM/DD/YY}} at {{date-expiry-HH:MM}}. Please accept, reject, or request a change before that.</p>
        </section>
<section>


${htmlButtons}


</section>
<section>
<table class="main-table" cellpadding="0" cellspacing="0" border="0" width="100%" style="box-sizing: border-box;">
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; background: #FFFFFF; box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.14); border-radius: 2px; margin-top: 12px;"  >
  <h1 class="ntg-email-section-title ntg-email-size--16" style="box-sizing: border-box; font-size: 16px; margin: 16px;">Quote details</h1>
</td>
</tr>
<tr>
                <td colspan="3" style="background: #FFFFFF; box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.14); border-radius: 2px; margin-top: 12px;">
<table role="grid" class="ntg-email-table" style="" width="100%">
  <thead class="p-datatable-thead" style="box-sizing: border-box;">
    <tr style="box-sizing: border-box;">
      <th style="" align="left" bgcolor="#F8F9FA"><b style="box-sizing: border-box;">                    Quote #</b></th>      
      <th style="" align="left" bgcolor="#F8F9FA"><b style="box-sizing: border-box;">                    Quote expires</b></th>     
      <th style="" align="left" bgcolor="#F8F9FA"><b style="box-sizing: border-box;">                    Ship Date</b></th>          
      <th style="" align="left" bgcolor="#F8F9FA"><b style="box-sizing: border-box;">Origin</b></th>          
      <th style="" align="left" bgcolor="#F8F9FA"><b style="box-sizing: border-box;">Destination</b></th>          
      <th style="" align="left" bgcolor="#F8F9FA"><b style="box-sizing: border-box;">Shipment Service</b></th>          
      <th style="" align="left" bgcolor="#F8F9FA"><b style="box-sizing: border-box;">Equipment</b></th>          
    </tr>
  </thead>
      
  <tbody class="p-datatable-tbody" style="box-sizing: border-box;">
    <tr style="">
      <td style="" align="left"><a href="{{quote-link}}">{{quote-id}}</a></td>
            
      <td style="" align="left">{{quote-expires}}</td>
      <td style="" align="left">{{ship-date}}</td>
      <td style="" align="left">{{location[origin]}}</td>
      <td style="" align="left">{{location[destination]}}</td>
      <td style="" align="left">{{shipment-service}}</td>
      <td style="" align="left">{{equipment-group}}</td>
    </tr>
  </tbody>
</table>
</td>
</tr>
</table>
<br>
<br>
<table class="quote-attribute">
<td class="quote-attribute--label">Line Haul Rate + Fuel</td>
<td class="quote-attribute--value">{{line-haul-rate}}</td>
</table>
<table class="quote-attribute">
<td class="quote-attribute--label">Accessorials Fees</td>
<td class="quote-attribute--value">{{accessorials-fees}}</td>
</table>
</section>
<section>
<table class="quote-attribute">
<td class="quote-attribute--label">Total Rate(Incl Accessorials)</td>
<td class="quote-attribute--value"><strong>{{total-rate}}</strong></td>
</table>
</section>
<br>
<section>

<table class="main-table" cellpadding="0" cellspacing="0" border="0" width="100%" style="box-sizing: border-box;">
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; background: #FFFFFF; box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.14); border-radius: 2px; margin-top: 12px;"  >
  <h1 class="ntg-email-section-title ntg-email-size--16" style="box-sizing: border-box; font-size: 16px; margin: 16px;">Product Details</h1>
</td>
</tr>
<tr>
<td colspan="3" style="background: #FFFFFF; box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.14); border-radius: 2px; margin-top: 12px;">
<table role="grid" class="ntg-email-table" style="" width="100%">
  <thead class="p-datatable-thead" style="box-sizing: border-box;">
    <tr style="box-sizing: border-box;">
      <th style="width:70%;" align="left" bgcolor="#F8F9FA"><b style="box-sizing: border-box;">Product category</b></th>      
      <th style="" align="left" bgcolor="#F8F9FA"><b style="box-sizing: border-box;">Weight</b></th>          
    </tr>
  </thead>
      
  <tbody class="p-datatable-tbody" style="box-sizing: border-box;">
<!--    <tr style="">-->
<!--      <td style="width:70%;" align="left">{{product-category}}</td>-->
<!--            -->
<!--      <td style="" align="left">{{product-category-weight}}</td>-->
<!--    </tr>-->
  </tbody>
</table>
</td>
</tr>
</table>
<br>
</section>

<section>
<table class="quote-attribute">
<td class="quote-attribute--label">Special Requirements</td>
<td class="quote-attribute--value"><strong>{{special-requirements}}</strong></td>
</table>
</section>
<br>


<section>




<section>

<table class="main-table" cellpadding="0" cellspacing="0" border="0" width="100%" style="box-sizing: border-box;">
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; background: #FFFFFF; box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.14); border-radius: 2px; margin-top: 12px;"  >
  <h1 class="ntg-email-section-title ntg-email-size--16" style="box-sizing: border-box; font-size: 16px; margin: 16px;">Accessorials Included in Quote *</h1>
</td>
</tr>
<tr>
<td colspan="3" style="background: #FFFFFF; box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.14); border-radius: 2px; margin-top: 12px;">
<table role="grid" class="ntg-email-table" style="" width="100%">
  <thead class="p-datatable-thead" style="box-sizing: border-box;">
    <tr style="box-sizing: border-box;">
      <th style="width:70%;" align="left" bgcolor="#F8F9FA"><b style="box-sizing: border-box;">Accessorial</b></th>      
      <th style="" align="left" bgcolor="#F8F9FA"><b style="box-sizing: border-box;">Value</b></th>          
    </tr>
  </thead>
      
  <tbody class="p-datatable-tbody" style="box-sizing: border-box;">
<!--    <tr style="">-->
<!--      <td style="width:70%;" align="left">{{accessorial[][name]} </td>-->
<!--            -->
<!--      <td style="" align="left">{{accessorial[][price]}}</td>-->
<!--    </tr>-->
  </tbody>
</table>
</td>
</tr>
</table>
<br>
</section>



</section>
<section>

<div class="table-container">
    <div class="table-container--header">
NTG Standard Accessorial Fees *
    </div>
    <table>
        <thead>
            <tr>
                <td style="width:70%;">
Accessorial
                </td>
                <td>
Value
                </td>
                
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:70%;">{{accessorial[][name]}</td>
<td class="">{{accessorial[][price]}}</td>
            </tr>
        </tbody>
    </table>
</div>
</section>
<section>
<div class="sidenote">* These are minimum charges, actual charges may be higher.</div>
</section>


${htmlButtons}

<section>
<div class="sidenote"><p>If you require further clarification, please do not hesitate to contact us anytime. <br>We look forward to hear from you soon.</p>
<p>Regards, <br>
<strong>NTG Sales Team, {{sales-rep--full-name}}</strong><br>
Email: <a href="mailto:salesrep@ntgfreight.com">{{sales-rep--email}}</a><br>
Phone: <a href="#">{{sales-rep--phone}}</a>
</p></div>
</section>
</div>
</td>
</tr>
</table>
      </body>
    </html>`;






    theHtml = `
<table class="body" cellpadding="0" cellspacing="0" border="0" width="100%" style="box-sizing: border-box; background: #FAFAFA; width: 100%; font-family: Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';">
  <tr align="center" style="box-sizing: border-box;">
    <td class="container" style="box-sizing: border-box; align-content: center; width: 930px; font-family: Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';" width="930">
    <table>
    <tr align="left">
    <td style="width: 90%;" width="90%">
    
      <div class="content" style="box-sizing: border-box; margin-top: 20px;"><div>
    <table style="display:flex">
        <tbody><tr><td style="height:67px;max-width:60px;width:60px">

            <img style="height:67px;max-width:60px;width:100%" width="60" height="auto" src="https://ci6.googleusercontent.com/proxy/9xcChkQxNmg7-u5VdUak3RzDtaiaJWnkvBi5mFR_cOchTMTvm4SQAYFnoy810UAQ1O21COfkGEUnNC7kB2kvVvDozgKjfUbrVVg7TSz5bSddCFCDe0Qej-0=s0-d-e1-ft#https://ntgfreight.com/wp-content/uploads/2018/09/NTG-high-res-png.png" class="CToWUd">
        </td>
        <td style="padding-left:15px">
            <br>
            <br>
            <h3 style="color:#797979;font-family:Roboto,'Helvetica Neue',sans-serif">Request for Quotation</h3>
        </td></tr>
    </tbody></table>
    <section style="font-family:Roboto,'Helvetica Neue',sans-serif;margin-bottom:30px;margin-top:30px">
        <p>Dear <strong>VERITIV</strong>, Thanks for your interest in NTG! </p>
        <p>
           Below are the details of your quote request. Your quote will expire on 04/26/2021 at 07:40 EST. 
           Please accept, reject, or request a change before that.
        </p>
    </section>
    <section style="font-family:Roboto,'Helvetica Neue',sans-serif;margin-bottom:30px;margin-top:30px">
        <div style="font-family:Roboto,'Helvetica Neue',sans-serif;margin-bottom:30px;margin-top:30px">
            <table>
                <tbody><tr><td>
                    <div>
                        <!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="mailto:salesrep@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%20%7B%7Bquote-id%7D%7D%20%3CApproved%3E&body=Approved%0D%0A" style="height:36px;v-text-anchor:middle;width:120px;" arcsize="13%" strokecolor="#7B5456" fillcolor="#7B5456">
    <w:anchorlock/>
    <center style="color:#ffffff;font-family:sans-serif;font-size:13px;font-weight:bold;">APPROVE</center>
  </v:roundrect>
<![endif]--><!--[if !mso]>-->
                        <a href="mailto:BRIAN.WORK@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%201259%20%3CApproved%3E&amp;body=Approved%0D%0A" style="background-color:#7b5456;border:2px solid #7b5456;border-radius:5px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:36px;text-align:center;text-decoration:none;width:90px" target="_blank">ACCEPT</a><!--<![endif]-->
                    </div>
                </td>
                <td style="padding-left:5px">
                    <div>
                        
                        <a href="mailto:BRIAN.WORK@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%201259%20%3CRejected%3E&amp;body=Rejected%0D%0A" style="background-color:transparent;border:2px solid #7b5456;border-radius:5px;color:#7b5456;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:36px;text-align:center;text-decoration:none;width:100px" target="_blank">REJECT</a>
                    </div>
                </td>
                <td style="padding-left:5px">
                    <div>
                        
                        <a href="mailto:BRIAN.WORK@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%201259%20%3CRequested%20Change%3E&amp;body=Requested%20change%0D%0A" style="white-space:nowrap;background-color:transparent;border:2px solid #7b5456;border-radius:5px;color:#7b5456;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:185px" target="_blank">
                            REQUEST CHANGE
                        </a>
                    </div>
                </td></tr>
            </tbody></table>
        </div>
    </section>

    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="box-sizing:border-box">
        <tbody><tr style="box-sizing:border-box">
        <td style="box-sizing:border-box;background:#ffffff;border-radius:2px;margin-top:12px">
        <h1 style="box-sizing:border-box;font-size:16px;margin:16px">Quote Details</h1>
        </td>
        </tr>
        <tr>
            <td colspan="3" style="background:#ffffff;border-radius:2px;margin-top:12px">
            <table role="grid" style="border-collapse:collapse;box-sizing:border-box;width:100%" width="100%">
                <thead style="box-sizing:border-box">
                    <tr style="box-sizing:border-box">
                        <th style="background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                            <b style="box-sizing:border-box">Quote #</b>
                        </th>      
                        <th style="background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                            <b style="box-sizing:border-box">Quote GUID</b>
                        </th>      
                        <th style="background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                            <b style="box-sizing:border-box">Quote Expires</b>
                        </th>     
                        <th style="background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                            <b style="box-sizing:border-box">Ship Date</b>
                        </th>          
                        <th style="background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                            <b style="box-sizing:border-box">Origin</b>
                        </th>          
                        <th style="background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                            <b style="box-sizing:border-box">Destination</b>
                        </th>          
                        <th style="background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                            <b style="box-sizing:border-box">Shipment Service</b>
                        </th>          
                        <th style="background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                            <b style="box-sizing:border-box">Equipment</b>
                        </th>          
                    </tr>
                </thead>
                <tbody style="box-sizing:border-box">
                    <tr style="background:#ffffff;box-sizing:border-box;color:#1c1c1c;outline-color:rgba(0,0,0,0.12)">
                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                        1259
                    </td>
                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                        9d967f8aa
                    </td>
                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                        04/26/2021 07:40 EST
                    </td>
                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                        04/26/2021
                    </td>
                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                        Georgetown, NY
                    </td>
                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                        Clearwater, FL
                    </td>
                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                        Full Truckload
                    </td>
                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                        FTL Dry Van
                    </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
    </tbody></table>
    <br>
    <table style="display:flex;margin-top:10px">
        <tbody><tr><td style="width:210px">Line Haul Rate</td>
        <td>$1000.00</td></tr>
    </tbody></table>
    <table style="display:flex;margin-top:10px">
        <tbody><tr><td style="width:210px">Accessorials Fees</td>
        <td>$0.00</td></tr>
    </tbody></table>
    
                        
    <section style="font-family:Roboto,'Helvetica Neue',sans-serif;margin-bottom:30px;margin-top:30px">
        <table style="display:flex">
            <tbody><tr><td style="width:210px">Total Rate(Incl Accessorials)</td>
            <td><strong>$1000.00</strong></td></tr>
        </tbody></table>
    </section><span class="im">
    <section style="font-family:Roboto,'Helvetica Neue',sans-serif;margin-bottom:30px;margin-top:30px">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="box-sizing:border-box">
            <tbody><tr style="box-sizing:border-box">
            <td style="box-sizing:border-box;background:#ffffff;border-radius:2px;margin-top:12px">
            <h1 style="box-sizing:border-box;font-size:16px;margin:16px">Product Details</h1>
            </td>
            </tr>
            <tr>
                <td colspan="3" style="background:#ffffff;border-radius:2px;margin-top:12px">
                <table role="grid" style="border-collapse:collapse;box-sizing:border-box;width:100%" width="100%">
                    <thead style="box-sizing:border-box">
                        <tr style="box-sizing:border-box">
                            <th style="width: 60%; background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                                <b style="box-sizing:border-box">Product Category</b>
                            </th>      
                            <th style="background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                                <b style="box-sizing:border-box">Weight</b>
                            </th>
                        </tr>
                    </thead>
                    <tbody style="box-sizing:border-box">
                        </tbody></table></td></tr>
                    
                </tbody></table>
                
            
        
    </section>
    <section style="font-family:Roboto,'Helvetica Neue',sans-serif;margin-bottom:30px;margin-top:30px">

    </section>
    <section style="font-family:Roboto,'Helvetica Neue',sans-serif;margin-bottom:30px;margin-top:30px">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="box-sizing:border-box">
            <tbody><tr style="box-sizing:border-box">
            <td style="box-sizing:border-box;background:#ffffff;border-radius:2px;margin-top:12px">
            <h1 style="box-sizing:border-box;font-size:16px;margin:16px">Accessorials Included in Quote *</h1>
            </td>
            </tr>
            <tr>
                <td colspan="3" style="background:#ffffff;border-radius:2px;margin-top:12px">
                <table role="grid" style="border-collapse:collapse;box-sizing:border-box;width:100%" width="100%">
                    <thead style="box-sizing:border-box">
                        <tr style="box-sizing:border-box">
                            <th style="width:60%;background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                                <b style="box-sizing:border-box">Accessorial</b>
                            </th>      
                            <th style="background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                                <b style="box-sizing:border-box">Value</b>
                            </th>
                        </tr>
                    </thead>
                    <tbody style="box-sizing:border-box">
                        </tbody></table></td></tr>
                    
                </tbody></table>
                
            
        
    </section>
    </span><section style="font-family:Roboto,'Helvetica Neue',sans-serif;margin-bottom:30px;margin-top:30px">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="box-sizing:border-box">
            <tbody><tr style="box-sizing:border-box">
            <td style="box-sizing:border-box;background:#ffffff;border-radius:2px;margin-top:12px">
            <h1 style="box-sizing:border-box;font-size:16px;margin:16px">NTG Standard Accessorial Fees *</h1>
            </td>
            </tr>
            <tr>
                <td colspan="3" style="background:#ffffff;border-radius:2px;margin-top:12px">
                <table role="grid" style="border-collapse:collapse;box-sizing:border-box;width:100%" width="100%">
                    <thead style="box-sizing:border-box">
                        <tr style="box-sizing:border-box">
                            <th style="width:60%;background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                                <b style="box-sizing:border-box">Accessorial</b>
                            </th>      
                            <th style="background:#f8f9fa;background-color:#f8f9fa;border:1px solid #e9ecef;border-bottom:2px solid #dbdbdb;border-left:none;border-right:none;border-top:1px solid #dbdbdb;border-width:1px 0 1px 0;box-sizing:border-box;color:#121212;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.75rem;font-weight:normal;line-height:1rem;padding:18px 8px;text-align:left;text-transform:capitalize" align="left" bgcolor="#F8F9FA">
                                <b style="box-sizing:border-box">Value</b>
                            </th>
                        </tr>
                    </thead>
                    <tbody style="box-sizing:border-box">
                                <tr style="background:#ffffff;box-sizing:border-box;color:#1c1c1c;outline-color:rgba(0,0,0,0.12)">
                                    <td style="width:60%;border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        DETENTION CHARGE </td>
                                    
                                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        $40.00</td>
                                </tr>
                                <tr style="background:#ffffff;box-sizing:border-box;color:#1c1c1c;outline-color:rgba(0,0,0,0.12)">
                                    <td style="width:60%;border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        DRIVER ASSIST FEE </td>
                                    
                                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        $50.00</td>
                                </tr>
                                <tr style="background:#ffffff;box-sizing:border-box;color:#1c1c1c;outline-color:rgba(0,0,0,0.12)">
                                    <td style="width:60%;border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        DROP TRAILER FEE </td>
                                    
                                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        $250.00</td>
                                </tr>
                                <tr style="background:#ffffff;box-sizing:border-box;color:#1c1c1c;outline-color:rgba(0,0,0,0.12)">
                                    <td style="width:60%;border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        LAYOVER CHARGE </td>
                                    
                                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        $250.00</td>
                                </tr>
                                <tr style="background:#ffffff;box-sizing:border-box;color:#1c1c1c;outline-color:rgba(0,0,0,0.12)">
                                    <td style="width:60%;border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        LUMPER FEE </td>
                                    
                                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        Varies by job/location</td>
                                </tr>
                                <tr style="background:#ffffff;box-sizing:border-box;color:#1c1c1c;outline-color:rgba(0,0,0,0.12)">
                                    <td style="width:60%;border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        PALLET EXCHANGE </td>
                                    
                                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        Varies by job/location</td>
                                </tr>
                                <tr style="background:#ffffff;box-sizing:border-box;color:#1c1c1c;outline-color:rgba(0,0,0,0.12)">
                                    <td style="width:60%;border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        TONU </td>
                                    
                                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        $150.00</td>
                                </tr>
                                <tr style="background:#ffffff;box-sizing:border-box;color:#1c1c1c;outline-color:rgba(0,0,0,0.12)">
                                    <td style="width:60%;border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        PALLET JACK </td>
                                    
                                    <td style="border:none;border-width:0 0 1px 0;box-sizing:border-box;font-family:Nunito,Roboto,-apple-system,BlinkMacSystemFont,'Segoe UI',Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:0.875rem;line-height:1.188rem;padding:18px 8px;text-align:left" align="left">
                                        $50.00</td>
                                </tr>
                        </tbody></table></td></tr>
                    
                </tbody></table>
                
            
        
    </section><span class="im">
    <section style="font-family:Roboto,'Helvetica Neue',sans-serif;margin-bottom:30px;margin-top:30px">
        <div>* These are minimum charges, actual charges may be higher.</div>
    </section>
    <div style="font-family:Roboto,'Helvetica Neue',sans-serif;margin-bottom:30px;margin-top:30px">
        <table>
            <tbody><tr><td>
                <div>
                    
                    <a href="mailto:BRIAN.WORK@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%201259%20%3CApproved%3E&amp;body=Approved%0D%0A" style="background-color:#7b5456;border:2px solid #7b5456;border-radius:5px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:90px" target="_blank">ACCEPT</a>
                </div>
            </td>
            <td style="padding-left:5px">
                <div>
                    
                    <a href="mailto:BRIAN.WORK@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%201259%20%3CRejected%3E&amp;body=Rejected%0D%0A" style="background-color:transparent;border:2px solid #7b5456;border-radius:5px;color:#7b5456;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:100px" target="_blank">REJECT</a>
                </div>
            </td>
            <td style="padding-left:5px">
                <div>
                    
                    <a href="mailto:BRIAN.WORK@ntgfreight.com?subject=Request%20for%20Quotation%20Quote%20nr%3A%201259%20%3CRequested%20Change%3E&amp;body=Requested%20change%0D%0A" style="white-space:nowrap;background-color:transparent;border:2px solid #7b5456;border-radius:5px;color:#7b5456;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:185px" target="_blank">
                        REQUEST CHANGE
                    </a>
                </div>
            </td></tr>
        </tbody></table>
    </div>
    <section style="font-family:Roboto,'Helvetica Neue',sans-serif;margin-bottom:30px;margin-top:30px">
        <div>
            <p>
                If you require further clarification, please do not hesitate to contact us anytime. <br>We
                look forward to hear from you soon.
            </p>
            <p>
            </p>
        </div>
    </section>

</span><img src="https://ci5.googleusercontent.com/proxy/Sq3W-Ri8GVZx2fpDta_NzmvTs4rZgq9iLfY6jTi991JSRUfSCZnp-kqF_ReXAhfL2BmWbY9Xpihdfsh16CpMFpzre3iw-6KNhqLnNzyIEcG6scQnZXsrYoVWY2ncoOduddCmCB_0ErI_dULmWm4VutAg9f4mRp2Bvm6EIuiyAstVjOCY6oonw68q_oClk2eTDybYu0e7RGYxKuQw1x-3JD5IJcNE4RkTBiTOnfA24gUu8XOf1G9fVcZlJ7bllNFT2h3Z6lAG25-RRcDtX05S2TfeLhd3zLJGw7gqoj2Vo1sB1jUeujPQt2fOd6YcfXQgIp4ZVxEkxfRXdeupj8u2_0gf5R7q_uBRtUqqvgvRw8hCr2tm3d5KwqOUieAAIidcgJ2v37SwiJ6cMsPO4A9uAmSUofiUp4BJ=s0-d-e1-ft#http://fhclick.ntgfreight.com/wf/open?upn=kDfL04j1Dwy-2B-2B1CtDGvhwyOZUAPdMfZ7FTDuIcIUgae7CWImHCBnJjG2PvEBiLq9YtyqcfJqdpKsHe2V6PaHKAdj08q6-2FM8nb6YSka-2BN1nJhMwcG2woQQ-2FXyccrY9RBH057k8iRQV4JFS9JyQpN91ry2AqlGuEutF9PEWqjCv7z9UMorejBmbEWcsWyxRwBdI5Yn7i-2FOROCbK3G6G4heHsFvTEScuLzkjm7ivzG9PJ0-3D" alt="" width="1" height="1" border="0" style="height:1px!important;width:1px!important;border-width:0!important;margin-top:0!important;margin-bottom:0!important;margin-right:0!important;margin-left:0!important;padding-top:0!important;padding-bottom:0!important;padding-right:0!important;padding-left:0!important" class="CToWUd"><div class="yj6qo"></div><div class="adL">
</div></div></div></td></tr></table></td></tr></table>`;

    inlineCss(theHtml, {
      url: 'https://sv-node-mailer.herokuapp.com/'
    }).then(function (html) {

      var mailOptions = {
        // from: process.env.OWNER_EMAIL,
        from: 'quotes@ntgfreight.com',
        to: req.body.email,
        subject: 'Request for Quotation',
        text: 'That was easy!',
        encoding: "utf-8",
        headers: {
          'X-MailType': 'my type',
        },
        html: html
      };

      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          html = `EMAIL SENT!<br><br>${html}`;
        }
      });

      res.send(html);
      res.end();
    }).catch((err) => {
      console.log('err o - ', err);
      res.end();
    })


  } else {

    res.writeHead(302, {
      'Location': '/'
    });
    res.end();
  }
})

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
